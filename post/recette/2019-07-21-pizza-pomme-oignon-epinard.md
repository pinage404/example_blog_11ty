---
layout: post
tags:
- recette
- plat
- vege
- vegan
authors:
- pinage404
- pin
date: 2019-07-21
title: Recette pizza pomme oignon épinard
---

## Ingredients

* pate à pizza
* pomme
* oignon et / ou échalotte
* feuilles d'épinard
* sauce tomate
* tomates

### Optionnels

* oeufs
* origan

## Ustensiles

* couteau
* four

## Préparation

1. étaler la sauce tomage sur la pate à pizza
2. couper la pomme en dés
3. étaler sur la pate à pizza
4. couper l'oignon et / ou l'échalotte en petits morçeaux
5. étaler sur la pate à pizza
6. couper les tomates en petits bouts
7. étaler sur la pate à pizza
8. laver les feuilles d'épinard
9. essorer les feuilles d'épinard
10. étaler sur la pate à pizza
11. mettre au four
12. (optionnel) _à mi-cuisson_ ajouter les oeufs
13. (optionnel) _après cuisson_ ajouter l'origan

**ASTUCE :** si vous mettez des oeufs étalez la garniture en laissant des trous pour éviter que les oeufs partent en voyage loin dans la pizza (au fond du four)
