---
layout: base
title: Avantages / Inconveinients
---

# {{ title }}

## Avantages

* simple
* rapide
* amélioration incrémentale
* out of the box :
  * fonctionne sans config
  * CLI simple et agréable
    * build par défault
    * watch
    * server
    * watch et server
  * autoreload
  * front matter data (metadata des fichiers)
    * système de templating + include
    * système de tags
* plein de langages supportés :
  * HTML <3
  * Markdown <3
  * Pug <3
  * JavaScript
  * JavaScript Template Literals
  * EJS
  * Liquid
  * Nunjucks
  * Handlebars
  * Mustache
  * Haml
* configurable par CLI (pour les options simples) ou par fichier
* documentation plutot claire
* syntaxe highlight (via plugin officiel)
* RSS (via plugin officiel)
* écrit en JS
* PWA (via plugin communautaire)
  * se base sur WorkBox
* ressemble à Jekyll

## Inconveinients

* documentation un peu trop simpliste pour faire des choses très customs dans la lib
* écrit en JS
  * pas trouvé de types
    * obligé d'aller voir la documentation pour voir ce qu'il est possible de faire dans la config
* toutes les fonctionnalités ne sont pas disponible avec Pug
  * parce que le langage ne le permet pas vraiment
  * on peut facilement contourner
* simple
  * il n'y a pas de theme par défaut
* il n'y a pas de notion d'auteurs
  * facilement contournable en lisant un peu la documentation et en écriant une petite fonction dans la configuration qui ajoute une collection
