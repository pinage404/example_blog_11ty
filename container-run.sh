#!/bin/sh
docker run \
	--rm \
	--interactive \
	--tty \
	--workdir /w \
	--volume $(pwd):/w:cached \
	--publish 8080:8080 \
	--publish 3001:3001 \
	--user node \
	node:12.6.0-alpine \
	"$@"
