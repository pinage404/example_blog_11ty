---
layout: post
tags:
- recette
- dessert
- vege
- vegan
authors:
- pinage404
date: 2019-07-20
title: Recette pomme au chocolat
---

## Ingredients

* pomme
* chocolat à cuisiner
* huile ou beurre

### Optionnels

* poudre de noisettes
* poudre d'amandes

## Ustensiles

* récipent micro-ondable
* un truc pour touiller (couteau, cuillère ...)

## Préparation

1. casse le chocolat à cuisiner dans le récipent
2. verser l'huile (ou ajouter le beurre)
3. laisser fondre 1m au micro-onde
4. (optionnel) ajouter la poudre
5. touiller
6. couper la pomme en quartiers
7. dépépiner
8. couper les quartiers
9. tremper une lamelle de pomme dans le mélanger
10. manger
11. recommencer à l'étape 9 jusqu'à épuisement
