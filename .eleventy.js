module.exports = eleventyConfig => {
	global.formatURL = eleventyConfig.javascriptFunctions.url;

	eleventyConfig.addCollection("post", collection =>
		collection.getFilteredByGlob("post/**/*")
	);

	eleventyConfig.addCollection("authors", collection => {
		const articlesByAuthor = new Map();

		const articlesWithAuthors = collection
			.getAllSorted()
			.filter(article => "authors" in article.data);

		for (const article of articlesWithAuthors) {
			const { authors } = article.data;

			for (const author of authors) {
				const articles = articlesByAuthor.get(author) || [];
				articlesByAuthor.set(author, articles.concat(article));
			}
		}

		return articlesByAuthor;
	});

	return {
		pathPrefix: "/example_blog_11ty/",
		dir: {
			layouts: "_layouts",
			output: "public"
		}
	};
};
